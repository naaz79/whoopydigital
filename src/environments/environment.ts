// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: true,
  baseUrl: window.location.origin,
  //apiUrl: 'http://15.185.184.195:8000/',
  //apiUrl: 'http://twassistant-env.eba-jmbu2ymp.ap-south-1.elasticbeanstalk.com',
  apiUrl: 'http://localhost:8000/',
  imageURL:'https://the-goldme-bucket.s3.me-south-1.amazonaws.com/'

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
