import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ApiConstant } from "src/app/_services/app-constant.enum";
import { NotificationService } from "src/app/notification.service";
import { AuthenticationService } from '../_services/authentication.service';
import { ActivatedRoute, Router, Params } from "@angular/router";


@Component({
  selector: 'app-home-dashboard',
  templateUrl: './home-dashboard.component.html',
  styleUrls: ['./home-dashboard.component.css']
})
export class HomeDashboardComponent implements OnInit {

  loader: boolean = false;
  stockLists = [];

  constructor(public authService: AuthenticationService,
    private httpClient: HttpClient,
    private _router: Router,
    private notifyService: NotificationService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    // alert("Abeeee");
    //this.getStockList();
  }

  // public getStockList(){

  //   console.log("Hello");
  //   this.loader = false;
  //   this.stockLists = []
  //   this.authService.getStockUrl().subscribe(res => {
          
  //         console.log("Response ..... ",res);
  //         if(res.status==true){
  //           // this.stockLists = res.data;
  //           this.loader = false;
  //           this.notifyService.showSuccess("Data Load successfully !!", "");
  //         }else{
  //           this.notifyService.showWarning("Something is wrong !!", "");
  //         }

  //   }, error => {
  //         this.notifyService.showError("Something went wrong!!...", "")
  //   });
  // }

}
