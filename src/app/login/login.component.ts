import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '.././_services/authentication.service';
import { Router } from '@angular/router';
import { LocationStrategy } from '@angular/common';
import { NotificationService } from '.././notification.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public authService: AuthenticationService, public router: Router,private notifyService : NotificationService) {

  }

  public phoneNo: string = '9108650221';
  public password: string = '123456';
  public warningMessage: string;

  ngOnInit(): void {
  }

  onLogIn() {

    if(!this.phoneNo || !this.password){
      this.warningMessage = 'Please Enter Login Details';
    }else{

      this.authService.login(this.phoneNo, this.password)
        .subscribe(res => {
          if(res.status==true){
            localStorage.setItem("isLogined","true");
            if(res.token) {
               sessionStorage.setItem('token', res.data.token);
             }
            this.notifyService.showSuccess("Login successfully !!", "");
            this.router.navigate(['/home']);
          }else{
            this.notifyService.showError("Invalid Credentials !!", "")
          }

          }, error => {
           //console.log(error)
          this.notifyService.showError("Something went wrong!!...", "")
        });
    }
  }

}
