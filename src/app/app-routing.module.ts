import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { UsersettingComponent } from './pages/user/usersList/userList.component';
import { SpecUserDetailsComponent } from './pages/user/spec-user-details/spec-user-details.component';
import { EditUserComponent } from './pages/user/edit-user/edit-user.component';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { NotificationComponent } from './pages/notification/notification.component';

import {InstagramPagesListComponent} from './pages/instagram-pages-list/instagram-pages-list.component'

 const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeDashboardComponent },
  { path: 'sendNotification', component: NotificationComponent },
  { path: 'tUserList', component: InstagramPagesListComponent },
  // { path: '', component: LoginComponent },
  
  { path: 'userlist',canActivate:[AuthGuard],component:UsersettingComponent},
  { path: 'specificuser/:id',canActivate:[AuthGuard],component:SpecUserDetailsComponent},
  { path: 'edituser/:id',canActivate:[AuthGuard], component: EditUserComponent },
  { path: '**',canActivate:[AuthGuard],component: PagenotfoundComponent},
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
