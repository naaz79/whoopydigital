import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxShimmerLoadingModule } from  'ngx-shimmer-loading';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AuthenticationService } from './_services/index';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LoginComponent } from './login/login.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AuthGuard } from './auth.guard';
import { ToastrModule } from 'node_modules/ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { UsersettingComponent } from './pages/user/usersList/userList.component';
import { SpecUserDetailsComponent } from './pages/user/spec-user-details/spec-user-details.component';
import { ChartsModule } from 'ng2-charts';
import { CKEditorModule } from 'ckeditor4-angular';
import { EditUserComponent } from './pages/user/edit-user/edit-user.component';
import { DataTablesModule } from 'angular-datatables';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { NotificationComponent } from './pages/notification/notification.component';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { InstagramPagesListComponent } from './pages/instagram-pages-list/instagram-pages-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';






@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    LoginComponent,
    PagenotfoundComponent,
    UsersettingComponent,
    SpecUserDetailsComponent,
    EditUserComponent,
    HomeDashboardComponent,
    NotificationComponent,
    InstagramPagesListComponent
    
  ],
  imports: [
    ChartsModule,
    DataTablesModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
   
  
    NgxShimmerLoadingModule,
    CKEditorModule,
    BsDropdownModule.forRoot(),
    ToastrModule.forRoot({
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    BrowserAnimationsModule,
    AngularFileUploaderModule,
    NgbModule,
  ],
  exports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    NgxShimmerLoadingModule,
    
  ],
  providers: [
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
