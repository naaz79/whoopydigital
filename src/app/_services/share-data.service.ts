import { Injectable } from '@angular/core';
import { Subject,Observable } from 'rxjs';

@Injectable()
export class ShareDataService {

  constructor() { }

  private userProfile = new Subject<any>();

  public getUserProfile(): Observable<any> {
    return this.userProfile.asObservable();
  }
 
  public setUserProfile(message: any): void {
    this.userProfile.next(message);
  }
}
