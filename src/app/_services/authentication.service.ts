import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { ApiConstant } from "src/app/_services/app-constant.enum";
import * as XLSX from 'xlsx'; 



@Injectable({ providedIn: 'root' })

export class AuthenticationService {

  public token: string;
  public headers: HttpHeaders;
  public readonly apiUrl = environment.apiUrl;2222222
  public readonly baseUrl = environment.baseUrl;

  constructor(public http: HttpClient) {
  }
  httpOptions = {

    headers: new HttpHeaders({
      'Content-Type': 'application/json'
     })
  };


  login(phoneNo: string, password: string): Observable<any> {
    const loginUrl = ApiConstant.loginUrl;
    var raw = JSON.stringify({ phoneNo: phoneNo, password: password });
    return this.http.post(loginUrl, JSON.parse(raw)).pipe(map((response: Response) => {
               return response;
    }));
  }

  getStockUrl(): Observable<any> {
    const stockListUrl = ApiConstant.stockListUrl;
    return this.http.get(stockListUrl).pipe(map((response: Response) => {
      return response;
    }));
  }


  sendTelegramNotification(requestBody: any): Observable<any> {
    const sendMessageApi = ApiConstant.sendMessageApi;
    return this.http.post(sendMessageApi,requestBody).pipe(map((response: Response) => {
      return response;
    }));
  }

  getUserLists(pageCategory): Observable<any> {
    /*
    const clientListAPi = ApiConstant.clientListAPi;
    return this.http.get(clientListAPi).pipe(map((response: Response) => {
      return response;*/
     /* return this.http.get("http://localhost:8000/pagesList/Hindi_Page")*/
     const instagramPageApi = ApiConstant.instagramPageApi;
      return this.http.get(instagramPageApi+pageCategory)
      .pipe(map((res:any)=>{
        return res['data'];
    }));
  }
  
  
  

  updateClients(): Observable<any> { // Sync User
    const updateClientData = ApiConstant.updateClients;
    return this.http.get(updateClientData).pipe(map((response: Response) => {
      return response;
    }));
  }


  updateClientProfile(requestBody : any, id:string) {
   /*const updateClientDetails = ApiConstant.updateClientDetails;*/
    const updateClientDetails = ApiConstant.editUrl;
    return this.http.post<any>(updateClientDetails+id,requestBody).pipe(map((response: Response) => {
      return response;
    }));
  }

  
  /*updateClientProfile(requestBody : any): Observable<any> {
    const updateClientDetails = ApiConstant.updateClientDetails;
     return this.http.post("http://localhost:8000/pagesList/set/", JSON.parse(requestBody)).pipe(map((response: Response) => {
       return response;
     }));
   }*/
 
 
  

























  // Old Code Need to remove 

  // getProductDetails(parameter: string): Observable<any> {
  //   const url1 = ApiConstant.getProductListByIdURL;
  //   return this.http.get(url1+parameter).pipe(map((response: Response) => {
  //     return response;
  //    }));
  // }
  

  // getOrderDetails(parameter: string): Observable<any> {
  //   const baseUrl = ApiConstant.getOrderDetailsURL;
  //   return this.http.get(baseUrl+parameter).pipe(map((response: Response) => {
  //     return response;
  //   }));
  // }


  // updateOrderRefundStatus(raw: any): Observable<any> {
  //   const url = ApiConstant.updateRefundStatus;
  //   return this.http.post(url, JSON.parse(raw)).pipe(map((response: Response) => {
  //      return response;
  //   }));
  // }
  
 

 


  /* Export Option  */
  exportexcel(tableName:string,fileName:string): void 
  {
      /* table id is passed over here */   
      let element = document.getElementById(tableName); 
      const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

      /* generate workbook and add the worksheet */
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      /* save to file */
      XLSX.writeFile(wb, fileName);
     
 }

// getDashboard(): Observable<any> {

//   var token =  sessionStorage.getItem('token');
//   var header = {
//     headers: new HttpHeaders()
//      .set('x-access-token',  `Bearer ${token}`)
//   }

//   return this.http.get(this.apiUrl+"getDashboard",header)
//       .pipe(
//           map((response: Response) => {
//              console.log(response);
//              return response;
//           })
//       );
// }

// getProductList(): Observable<any> {

//   var token =  sessionStorage.getItem('token');
//   var header = {
//     headers: new HttpHeaders()
//      .set('x-access-token',  `Bearer ${token}`)
//   }

//   return this.http.get(this.apiUrl+"product",header)
//       .pipe(
//           map((response: Response) => {
//              console.log(response);
//              return response;
//           })
//       );
// }

}
