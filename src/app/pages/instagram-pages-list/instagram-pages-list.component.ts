import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { NotificationService } from "src/app/notification.service";
import { Observable, Subject } from 'rxjs';
import * as xlsx from "xlsx";
import { forEach } from 'jszip';


@Component({
  selector: 'app-instagram-pages-list',
  templateUrl: './instagram-pages-list.component.html',
  styleUrls: ['./instagram-pages-list.component.css']
})
export class InstagramPagesListComponent implements OnInit {

  loader: boolean = false;
  totalFollowers=0;
  newText:string;
  userLists = [];
  selectedClient : any;
  
  fileName="export-data.xlsx";
  fileName2="export2-data.xlsx";
  ExcelList=[];
  firstName:string;
  selectedOption : any = "All";

  chatDetailsObj = {
    id: "",
    pageName: "",
    followers: "",
    pageCategory: "",
    link: "",
}

  constructor(public authService: AuthenticationService,
    private httpClient: HttpClient,
    private _router: Router,
    private notifyService: NotificationService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.getAvailablePage(this.selectedOption);
    
    
}

  
public onOptionsSelected(event) {
    const value = event.target.value;
    this.selectedOption = value;
    this.getAvailablePage(this.selectedOption);
}


public getAvailablePage(pageCategory){

    this.loader = false;
    this.totalFollowers=0;
    this.authService.getUserLists(pageCategory).subscribe(res => {
      this.userLists = [];
      this.userLists = res;
      let tableData=$("#excel-table").DataTable();
      tableData.destroy();
       setTimeout(() => {
        $("#excel-table").DataTable();
      }, 
      );
       
      
    for (var i = 0; i < this.userLists.length; i++) {
        this.totalFollowers =this.totalFollowers+this.getNumber(this.userLists[i].followers);
    }

    

      
    let string=this.totalFollowers.toString().length;

    if (string==9) {
      let text=this.totalFollowers/1000000;
      let fixed=text.toFixed(2)+"M";
      this.newText=fixed;
      
    }
    else if(string==10){
      let text=this.totalFollowers/1000000000;
      let fixed=text.toFixed(2)+"B";
      this.newText=fixed;

    }

    else if(string==8){
      let text=this.totalFollowers/1000000;
      let fixed=text.toFixed(2)+"M";
      this.newText=fixed;

    }
    else if(string==7){
      let text=this.totalFollowers/1000000;
      let fixed=text.toFixed(2)+"M";
      this.newText=fixed;
    }

    else if(string==6){
      let text=this.totalFollowers/100000;
      let fixed=text.toFixed(2)+"k";
      this.newText=fixed;

    }
    
    this.notifyService.showSuccess("Data Load successfully !!", "");
          
    }, error => {
          this.notifyService.showError("Something went wrong!!...", "")
    });

}

  



public getNumber(folloers){
  
  if(folloers.includes('k') || folloers.includes('K')){ 
      
      
      var number = folloers;
      if(number.includes('.')){
            number = number.replace(".", "");
            var num=number.substring(0, number.length - 1);
            var str1=num*100;
            //console.log(str1);
            return str1;
            
      }
      
      else{
        var str = number.substring(0, number.length - 1);
        var finalNumber=str*1000;
        
        //console.log(finalNumber);
        
        return finalNumber;
        
        
      }
      
      
      
      
  }
  
  
  else if(folloers.includes('m') || folloers.includes('M')){ 
      
      var number = folloers;
      if(number.includes('.')){
            number = number.replace(".", "");
            var num=number.substring(0, number.length - 1);
            var str1=num*100000;
            return str1;
      }
      
      else{
        var str = number.substring(0, number.length - 1);
        var finalNumber=str*1000000;
        
        //console.log(finalNumber);
        
        return finalNumber;
        
        
      }
  
  }
  
}
  
   
  
  


public exportExcel():void{
    const ws:xlsx.WorkSheet=xlsx.utils.json_to_sheet(this.userLists);
    const wb:xlsx.WorkBook=xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb,ws,"sheet1");
    xlsx.writeFile(wb,this.fileName);

}



  public onChange($event){
    const id=$event.target.value;
    console.log(id);
    this.userLists.forEach((item,index)=>{
      if(item.id==id){
        var obj={
          id:item.id,
          pageName: item.pageName,
          followers: item.followers,
          pageCategory: item.pageCategory,
          link:item.link

        }
        this.ExcelList.push(obj);
      }
      
    })

    console.log(JSON.stringify(this.ExcelList));

    
  
}


  public selectedExcel(){
    const ws:xlsx.WorkSheet=xlsx.utils.json_to_sheet(this.ExcelList);
    const wb:xlsx.WorkBook=xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb,ws,"sheet1");
    xlsx.writeFile(wb,this.fileName2);
    
  }
 
  /*public Search(){
    if(this.firstName != ""){
      this.userLists.filter(res=>{

        return res.firstName.toLocaleLowerCase().match(this.firstName.toLocaleLowerCase());
        

      })
      
    }
  
    else if(this.firstName==""){
      this.ngOnInit();
      
    }
    else if(this.firstName!=this.userLists.values){

      alert("not found")
      
      
    }

  }
   */



  public updateClient(){
    this.authService.updateClients().subscribe(res => {
          this.notifyService.showSuccess("Updates successfully !!", "");
    }, error => {
          this.notifyService.showError("Something went wrong!!...", "")
    });
  }


  public editUser(clientDetails : any){
    this.chatDetailsObj.id = clientDetails.id;
    this.chatDetailsObj.pageName = clientDetails.pageName;
    this.chatDetailsObj.followers = clientDetails.followers;
    this.chatDetailsObj.pageCategory = clientDetails.pageCategory;
    this.chatDetailsObj.link = clientDetails.link;
 }


public updateClientProfile(){

    this.authService.updateClientProfile(this.chatDetailsObj,this.chatDetailsObj.id).subscribe(res => {
            if(res){
              this.notifyService.showSuccess("Updates successfully !!", "");
              this.getAvailablePage(this.selectedOption);
              this.chatDetailsObj = {
                id: "",
                pageName: "",
                followers: "",
                pageCategory: "",
                link: "",
              }
              


            }
            else{
              this.notifyService.showError("Something went wrong!!...", "")
            }

      }, error => {
            this.notifyService.showError("Something went wrong!!...", "")
      });

}

}
