import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstagramPagesListComponent } from './instagram-pages-list.component';

describe('InstagramPagesListComponent', () => {
  let component: InstagramPagesListComponent;
  let fixture: ComponentFixture<InstagramPagesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstagramPagesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstagramPagesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
