import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ApiConstant } from "src/app/_services/app-constant.enum";
import { NotificationService } from "src/app/notification.service";
import { AuthenticationService } from '../../_services/authentication.service'
import { ActivatedRoute, Router, Params } from "@angular/router";
import {  WorkBook, read, utils, write, readFile } from 'xlsx';
import 'select2';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  loader: boolean = false;
  stockLists = [];
  telegramChatIdList : any;
  docTojsonData : any;
  arrayBuffer:any;
  file:File;
  errorMsg : "Hello World";
  notificationForm = {
    selectedStock: "",
    buyingRange: "",
    chartLink: "",
    note: ""
  }

  constructor(public authService: AuthenticationService,
    private httpClient: HttpClient,
    private _router: Router,
    private notifyService: NotificationService,
    private route: ActivatedRoute) { }


  ngOnInit(): void {

   // $('.js-example-basic-single').select2(); //initialize select2 to particular input

    this.getStockList();
  }

  public getStockList(){

    this.loader = false;
    this.stockLists = []
    this.authService.getStockUrl().subscribe(res => {
      
          this.stockLists = res;
          this.loader = false;
          //this.notifyService.showSuccess("Data Load successfully !!", "");
         
    }, error => {
          this.notifyService.showError("Something went wrong!!...", "")
    });
  }


  public sendNotification() {
      this.sendMessageApiCall()
  }

  public sendMessageApiCall(){


    var raw = JSON.stringify({ 
      security: this.notificationForm.selectedStock,
      priceRange: this.notificationForm.buyingRange,
      chartLink: this.notificationForm.chartLink,
      note: this.notificationForm.note,
      clientQuantity: JSON.stringify(this.docTojsonData)
    });

    this.authService.sendTelegramNotification(JSON.parse(raw)).subscribe(res => {
          this.loader = false;
          if(res.success){
            this.notifyService.showSuccess("Message Sent !!", "");
            location.reload();
          }else{
            this.notifyService.showError("Something went wrong!!...", "")
          }
    }, error => {
          this.notifyService.showError("Something went wrong!!...", "")
    });
  }



  public parseExcel(file) {
         
      let reader = new FileReader();
      let workbookkk;
      let XL_row_object;
      let json_object;
      reader.readAsBinaryString(file);
      const promise1 =  new Promise((resolve, reject) => {
        reader.onload = function(){
          let data = reader.result;
          workbookkk = read(data,{type: 'binary'});
          workbookkk.SheetNames.forEach(function(sheetName) {
            XL_row_object = utils.sheet_to_json(workbookkk.Sheets[sheetName]);
            //resolve(JSON.stringify(XL_row_object));
            resolve((XL_row_object));
          });
        };
      });

      promise1.then((value) => {
        this.docTojsonData = value;
        console.log(this.docTojsonData);
      });
  };

  public handleFileSelect(evt) {
      this.parseExcel(evt.target.files[0]);
  }
  
}
