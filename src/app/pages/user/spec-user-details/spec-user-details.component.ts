import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'src/app/notification.service';
import { ApiConstant } from 'src/app/_services/app-constant.enum';

@Component({
  selector: 'app-spec-user-details',
  templateUrl: './spec-user-details.component.html',
  styleUrls: ['./spec-user-details.component.css']
})
export class SpecUserDetailsComponent implements OnInit {
  loader: boolean = false;
  dataSource:any;
  url = ApiConstant.getResiterUserById;
  userOrderHostoryURL = ApiConstant.userOrderHostory;
  
  paramId;
  userDetail = [];
  orderHistory : [];
  constructor(
    private httpClient: HttpClient,
    private _actroute: ActivatedRoute,
    private notifyService: NotificationService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.loader = true;
    this.paramId = this._actroute.snapshot.params["id"];
    this.httpClient.get(this.url + this.paramId).subscribe((data: any) => {
        if (data != null) {
          this.dataSource = data;
          this.loader = false;
          this.notifyService.showSuccess("Data Load successfully !!", "");
        } else {
          this.notifyService.showError("Something is wrong !!", "");
        }
      },
      (err) => {
        this.notifyService.showError("Something is wrong !!", err);
      }
    );
    this.getOrderHistory();
  }

  getOrderHistory(){

    this.httpClient.get(this.userOrderHostoryURL + this.paramId).subscribe((data: any) => {
      if (data != null) {
        this.orderHistory = data.orderHistory;
        console.log(this.orderHistory);
        
      } else {
        this.notifyService.showError("Something is wrong While Fatching Order History!!", "");
      }
    },
    (err) => {
      console.log(err);
      this.notifyService.showError("Something is wrong !!", err);
    }
  );

  }

  editUser() {
    this._router.navigate(["/edituser",this.paramId]);
  }
  
}