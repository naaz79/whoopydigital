import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/notification.service';
import { ApiConstant } from 'src/app/_services/app-constant.enum';
import { AuthenticationService } from '../../../_services/authentication.service';

@Component({
  selector: 'app-usersetting',
  templateUrl: './userList.component.html',
  styleUrls: ['./userList.component.css']
})
export class UsersettingComponent implements OnInit {
  public loader: boolean = false;
  public dataSource = [];
  public storeList;
  p;
  searchText;
  url1 = ApiConstant.getAllResiterUserList;
  constructor(
    public authService: AuthenticationService,
    private httpClient: HttpClient,
    private _router: Router,
    private notifyService: NotificationService
  ) {}

  ngOnInit(): void {
    this.loadData();
    this.loader = true;
  }
  loadData() {
   
    this.httpClient.get(this.url1).subscribe((data: any) => {
      if (data != null) {
        this.loader = false;
        this.dataSource = data;
        this.notifyService.showSuccess("Data Load successfully !!", "");
      } else {
        this.notifyService.showWarning("Something is wrong !!", "");
      }
    });
  }

  onClickUser(item) {
    this._router.navigate(["/specificuser", item.userID]);
  }

  exportTable(): void 
  {
      this.authService.exportexcel('excel-table','userTable.xlsx');
  }
}


