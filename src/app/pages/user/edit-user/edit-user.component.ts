import { Component, OnInit } from '@angular/core';
import {ActivatedRoute , Router} from "@angular/router";
import { NotificationService } from 'src/app/notification.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ApiConstant } from 'src/app/_services/app-constant.enum';
import { FormGroup, FormControl, Validators } from "@angular/forms";


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  FileForm: FormGroup;
  userProfile:any;
  userID:any;
  url = ApiConstant.getResiterUserById;
  updateProfileUrl = ApiConstant.updateUserProfile;
  constructor(
    private httpClient: HttpClient,
    private _actroute: ActivatedRoute,
    private notifyService: NotificationService,
    private _router: Router,
    private activatedRoute: ActivatedRoute) {
   }

  ngOnInit(): void {

    this.FileForm = new FormGroup({
      userId: new FormControl(""),
      fullName: new FormControl(""),
      emailId: new FormControl(""),
      phoneNo: new FormControl(""),
      addressLine1: new FormControl(""),
      city: new FormControl(""),
      country: new FormControl("")
    });

    this.userID = this.activatedRoute.snapshot.params["id"];
    this.httpClient.get(this.url + this.userID).subscribe((data: any) => {
      if (data != null) {
        this.userProfile = data;

        this.FileForm.patchValue({
          userId: this.userID,
          fullName: this.userProfile.fullName,
          emailId: this.userProfile.email,
          phoneNo: this.userProfile.phoneNo,
          addressLine1: this.userProfile.addressLine1,
          city: this.userProfile.city,
          country: this.userProfile.country
        });
        this.notifyService.showSuccess("Data Load successfully !!", "");
      } else {
        this.notifyService.showError("Something is wrong !!", "");
      }
    },
    (err) => {
      this.notifyService.showError("Something is wrong !!", err);
    }
  );
    console.log(this.userID);
    
  }

  onEditClick() {

    let head = new HttpHeaders().set("Content-Type", "application/json");
    const body = JSON.stringify(this.FileForm.value);
    this.httpClient.post(this.updateProfileUrl, body, { headers: head }).subscribe((data: any) => {
        if (data != null) {
          this.notifyService.showSuccess("Edited successfully !!", "");
        } else {
          this.notifyService.showWarning("Something is wrong !!", "");
        }
    });
  }

}
