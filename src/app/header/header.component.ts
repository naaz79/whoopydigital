import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { NotificationService } from '../notification.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private _router:Router,private notifyService : NotificationService) { }

  ngOnInit(): void {

   var isLoggedIn =  localStorage.getItem("isLogined");
   if(isLoggedIn=="true"){

   }else{
     this._router.navigate(['/']);
   }

  }

  signOut():void
  {
    window.sessionStorage.removeItem("isLogined")
    this.notifyService.showSuccess("Logout successfully !!", "");
    this._router.navigate(['/']);
  }
}
