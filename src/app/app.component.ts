import { Component, OnDestroy, OnInit } from '@angular/core';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit,OnDestroy{

  constructor(
  ) {}
  
  title = 'Goldme';

  ngOnDestroy():void{
    //this.subscription.unsubscribe();
  }
  ngOnInit(): void {
    //this.subscription = this.shareDataService.getUserProfile().subscribe(msg => this.shareDataService = msg);
  }
}
